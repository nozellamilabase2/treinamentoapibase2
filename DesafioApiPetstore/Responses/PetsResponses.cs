using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioApiPetstore.Responses
{    
    public class Category    {
        public Category(int id, string name){
            this.id = id;
            this.name = name;
        }
        public int id { get; set; } 
        public string name { get; set; } 
    }

    public class Tag    {
        public Tag(int id, string name){
            this.id = id;
            this.name = name;
        }
        public int id { get; set; } 
        public string name { get; set; } 
    }

    public class PetsResponses    {
        public int id { get; set; } 
        public Category category { get; set; } 
        public string name { get; set; } 
        public List<string> photoUrls { get; set; } 
        public List<Tag> tags { get; set; } 
        public string status { get; set; } 
    }

    public class PetsResponsesByStatus    {
        public List<PetsResponses> petsResponses { get; set; } 
    }
}
