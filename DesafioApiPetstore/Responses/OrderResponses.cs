using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioApiPetstore.Responses
{
    public class OrderResponses    {
        public int id { get; set; } 
        public int petId { get; set; } 
        public int quantity { get; set; } 
        public DateTime shipDate { get; set; } 
        public string status { get; set; } 
        public bool complete { get; set; } 
    }
}