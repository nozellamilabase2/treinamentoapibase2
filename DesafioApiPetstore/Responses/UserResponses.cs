using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioApiPetstore.Responses
{
    public class CreateUserResponse    {
        public int code { get; set; } 
        public string type { get; set; } 
        public string message { get; set; } 
    }
    public class GetUserResponse{
        public int id { get; set; } 
        public string username { get; set; } 
        public string firstName { get; set; } 
        public string lastName { get; set; } 
        public string email { get; set; } 
        public string password { get; set; } 
        public string phone { get; set; } 
        public int userStatus { get; set; } 
    }
}