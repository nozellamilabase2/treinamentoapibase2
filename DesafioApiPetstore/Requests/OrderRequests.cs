using DesafioApiPetstore.Bases;
using DesafioApiPetstore.Bodys;

namespace DesafioApiPetstore.Requests.PetStore
{
    public class OrderRequests : RequestBase
    {
        public OrderRequests(RestSharp.Method methodParam, OrderBodys order)
        {
            url = "https://petstore.swagger.io/v2/";
            requestService = "/store/order/";
            method = methodParam;
            
            switch (method.ToString())
            {
                case "GET":
                    requestService = "/store/order/" + order.id.ToString();
                    break;
                case "POST":
                    setJsonBodyChanges(order);
                    break;
                case "DELETE":
                    requestService = "/store/order/" + order.id.ToString();
                    break;
            }
        }

        public void setJsonBodyChanges(OrderBodys order)
        {
            jsonBodyObject = order;
        }
    }
}
