using RestSharp;
using DesafioApiPetstore.Bases;
using DesafioApiPetstore.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using DesafioApiPetstore.Bodys;

namespace DesafioApiPetstore.Requests.PetStore
{
    public class PetsRequests : RequestBase
    {
        public PetsRequests(RestSharp.Method methodParam, PetsBodys pet, bool findByStatus = false)
        {
            url = "https://petstore.swagger.io/v2/";
            requestService = "/pet/";
            method = methodParam;
            
            switch (method.ToString())
            {
                case "GET":
                    if(findByStatus == false)
                    {
                        requestService = "/pet/" + pet.id.ToString();
                    }
                    else
                    {
                        parameters.Add("status", pet.status.ToString());
                        requestService = "/pet/findByStatus?";
                    }
                    break;
                case "POST":
                    setJsonBodyChanges(pet);
                    break;
                case "DELETE":
                    requestService = "/pet/" + pet.id.ToString();
                    break;
            }
        }

        public void setJsonBodyChanges(PetsBodys pet)
        {
            jsonBodyObject = pet;
        }
    }
}
