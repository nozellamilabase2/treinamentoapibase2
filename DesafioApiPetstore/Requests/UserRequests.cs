using RestSharp;
using DesafioApiPetstore.Bases;
using DesafioApiPetstore.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using DesafioApiPetstore.Bodys;

namespace DesafioApiPetstore.Requests.PetStore
{
    public class UserRequests : RequestBase
    {
        public UserRequests(RestSharp.Method methodParam, UserBodys user)
        {
            url = "https://petstore.swagger.io/v2/";
            requestService = "/user/";
            method = methodParam;
            
            switch (method.ToString())
            {
                case "GET":
                    requestService = "/user/" + user.username.ToString();
                    break;
                case "POST":
                    setJsonBodyChanges(user);
                    break;
                case "DELETE":
                    requestService = "/user/" + user.id.ToString();
                    break;
            }
        }

        public void setJsonBodyChanges(UserBodys user)
        {
            jsonBodyObject = user;
        }
    }
}
