﻿using NUnit.Framework;
using RestSharp;
using DesafioApiPetstore.Bases;
using DesafioApiPetstore.Requests.PetStore;
using System;
using DesafioApiPetstore.Bodys;
using DesafioApiPetstore.Helpers;
using DesafioApiPetstore.Responses;

namespace DesafioApiPetstore.Tests
{    
    [TestFixture]
    public class PetsTests : TestBase
    {

        PetsRequests petsRequests;
        PetsResponses petsResponses;
        JsonDeserializer jsonDeserializer = new JsonDeserializer();
        
        [Test]//Duvida generica, como os relatorios sao gerados e utilizados ao fim da execução de uma bateria de testes?
        public void CreatePet()
        {
            Random random = new Random();
            int id = random.Next(0, 1000);
            string name = "doggie";
            string status = "available";

            PetsBodys petsBodys = new PetsBodys(id, name, status);
            RestSharp.Method postMethod = Method.POST;
            
            string expectedStatusCode = "OK";

            petsRequests = new PetsRequests(postMethod, petsBodys);

            IRestResponse<dynamic> response = petsRequests.ExecuteRequest();

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());

            //Assert by getting the created pet
            RestSharp.Method getMethod = Method.GET;
            petsRequests = new PetsRequests(getMethod, petsBodys);

            response = petsRequests.ExecuteRequest();                
            petsResponses = jsonDeserializer.Deserialize<PetsResponses>(response);//Se, por exemplo, acabar trazendo um response body diferente do esperado pela classe PetsResponses. O Deserialize já trata a exceção?

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());
            Assert.AreEqual(petsBodys.id, petsResponses.id);  
        }
        
        [Test]//Duvida generica, como os relatorios sao gerados e utilizados ao fim da execução de uma bateria de testes?
        public void CreatePetWithDynamicParameters()
        {
            Random random = new Random();
            int id = random.Next(0, 1000);
            string name = $"doggie - {id}";
            string status = "available";

            PetsBodys petsBodys = new PetsBodys(id, name, status);
            RestSharp.Method postMethod = Method.POST;
            
            string expectedStatusCode = "OK";

            petsRequests = new PetsRequests(postMethod, petsBodys);

            IRestResponse<dynamic> response = petsRequests.ExecuteRequest();

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());

            //Assert by getting the created pet
            RestSharp.Method getMethod = Method.GET;
            petsRequests = new PetsRequests(getMethod, petsBodys);

            try{
                response = petsRequests.ExecuteRequest();                
                petsResponses = jsonDeserializer.Deserialize<PetsResponses>(response);//Se, por exemplo, acabar trazendo um response body diferente do esperado pela classe PetsResponses. O Deserialize já trata a exceção?
                
                Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());
                Assert.AreEqual(petsBodys.id, petsResponses.id);

            }catch(Exception e){
                //Perguntar a melhor forma de fazer isso e se esse tipo de asserção é comum
            }
        }

        [Test]
        public void GetPetById()
        {
            int id = 155;
            string name = "doggie";
            string status = "available";

            PetsBodys petsBodys = new PetsBodys(id, name, status);
            RestSharp.Method getMethod = Method.GET;
            
            string expectedStatusCode = "OK";

            petsRequests = new PetsRequests(getMethod, petsBodys);

            IRestResponse<dynamic> response = petsRequests.ExecuteRequest();
            petsResponses = jsonDeserializer.Deserialize<PetsResponses>(response);//Se, por exemplo, acabar trazendo um response body diferente do esperado pela classe PetsResponses. O Deserialize já trata a exceção?

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());
            Assert.AreEqual(petsResponses.name, name);            
        }
/*
        [Test]
        public void GetPetByStatus()
        {
            PetsResponsesByStatus petsResponsesByStatus = new PetsResponsesByStatus();
            int id = 0;
            string name = "doggie";
            string status = "available";

            PetsBodys petsBodys = new PetsBodys(id, name, status);
            RestSharp.Method getMethod = Method.GET;
            
            string expectedStatusCode = "OK";

            petsRequests = new PetsRequests(getMethod, petsBodys, true);

            IRestResponse<dynamic> response = petsRequests.ExecuteRequest();
            petsResponsesByStatus = jsonDeserializer.Deserialize<PetsResponsesByStatus>(response);//Se, por exemplo, acabar trazendo um response body diferente do esperado pela classe PetsResponses. O Deserialize já trata a exceção?

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());
            Assert.AreEqual(petsResponses.status, status);            
        }
        */
    }
}
