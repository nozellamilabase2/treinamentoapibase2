﻿using NUnit.Framework;
using RestSharp;
using DesafioApiPetstore.Bases;
using DesafioApiPetstore.Requests.PetStore;
using System;
using DesafioApiPetstore.Bodys;
using DesafioApiPetstore.Helpers;
using DesafioApiPetstore.Responses;

namespace DesafioApiPetstore.Tests
{    
    [TestFixture]
    public class OrderTests : TestBase
    {

        PetsRequests petsRequests;
        OrderRequests orderRequests;
        PetsResponses petsResponses;
        OrderResponses orderResponses;
        JsonDeserializer jsonDeserializer = new JsonDeserializer();
        
        [Test]
        public void CreateOrder()
        {
            //Create pet
            Random random = new Random();
            int id = random.Next(0, 1000);
            string name = "doggie";
            string status = "available";

            PetsBodys petsBodys = new PetsBodys(id, name, status);
            RestSharp.Method postMethod = Method.POST;
            
            string expectedStatusCode = "OK";

            petsRequests = new PetsRequests(postMethod, petsBodys);
            IRestResponse<dynamic> response = petsRequests.ExecuteRequest();

            petsResponses = jsonDeserializer.Deserialize<PetsResponses>(response);//Se, por exemplo, acabar trazendo um response body diferente do esperado pela classe PetsResponses. O Deserialize já trata a exceção?

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());

            //Create Order
            OrderBodys orderBodys = new OrderBodys(id, petsResponses.id);
            orderRequests = new OrderRequests(postMethod, orderBodys);

            response = orderRequests.ExecuteRequest();
            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());

            //Assert by getting the created order
            RestSharp.Method getMethod = Method.GET;
            orderRequests = new OrderRequests(getMethod, orderBodys);

            response = orderRequests.ExecuteRequest();                
            orderResponses = jsonDeserializer.Deserialize<OrderResponses>(response);//Se, por exemplo, acabar trazendo um response body diferente do esperado pela classe PetsResponses. O Deserialize já trata a exceção?
                
            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());
            Assert.AreEqual(orderResponses.petId, petsResponses.id);
        }       
    }
}
