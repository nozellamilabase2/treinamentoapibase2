﻿using NUnit.Framework;
using RestSharp;
using DesafioApiPetstore.Bases;
using DesafioApiPetstore.Requests.PetStore;
using System;
using DesafioApiPetstore.Bodys;
using DesafioApiPetstore.Helpers;
using DesafioApiPetstore.Responses;

namespace DesafioApiPetstore.Tests
{    
    [TestFixture]
    public class UsersTests : TestBase
    {
        UserRequests userRequests;
        CreateUserResponse createUserResponse;
        GetUserResponse getUserResponse; 
        JsonDeserializer jsonDeserializer;

        int id;
        
        [Test]
        public void CreateUser()
        {
            Random random = new Random();
            id = random.Next(1000);
            string username = $"username.{id}";

            UserBodys userBodys = new UserBodys(id, username);
            RestSharp.Method postMethod = Method.POST;
            
            string expectedStatusCode = "OK";

            userRequests = new UserRequests(postMethod, userBodys);

            IRestResponse<dynamic> response = userRequests.ExecuteRequest();

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());

            //Assert by getting the created user
            RestSharp.Method getMethod = Method.GET;
            userRequests = new UserRequests(getMethod, userBodys);

            try{
                response = userRequests.ExecuteRequest();                
                
                Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());
            }catch(Exception e){
                //Perguntar a melhor forma de fazer isso e se esse tipo de asserção é comum
            }
        }
        
        [Test]
        public void CreateUserWithDynamicParameters()
        {
            Random random = new Random();
            id = random.Next(0, 1000);
            string username = $"username.{id}";

            UserBodys userBodys = new UserBodys(id, username);
            RestSharp.Method postMethod = Method.POST;
            
            string expectedStatusCode = "OK";

            userRequests = new UserRequests(postMethod, userBodys);

            IRestResponse<dynamic> response = userRequests.ExecuteRequest();

            Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());

            //Assert by getting the created pet
            RestSharp.Method getMethod = Method.GET;
            userRequests = new UserRequests(getMethod, userBodys);

            try{
                response = userRequests.ExecuteRequest();                
                
                Assert.AreEqual(expectedStatusCode, response.StatusCode.ToString());
            }catch(Exception e){
                //Perguntar a melhor forma de fazer isso e se esse tipo de asserção é comum
            }
        }
    }
}
