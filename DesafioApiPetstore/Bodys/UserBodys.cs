using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioApiPetstore.Bodys
{
    public class UserBodys{
        public UserBodys(){}
        public UserBodys(int id, string username){
            this.id = id;
            this.username = username;
            this.firstName = "Name";
            this.lastName = "LastName";
            this.email = "email@email.com";
            this.password = "123456";
            this.phone = "3499531402";
            this.userStatus = 0;
        }
        public int id { get; set; } 
        public string username { get; set; } 
        public string firstName { get; set; } 
        public string lastName { get; set; } 
        public string email { get; set; } 
        public string password { get; set; } 
        public string phone { get; set; } 
        public int userStatus { get; set; } 
    }
}