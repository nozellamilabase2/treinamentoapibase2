using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioApiPetstore.Bodys
{
    public class OrderBodys    {
        public OrderBodys(){}

        public OrderBodys(int id, int petId){
            this.id = id;
            this.petId = petId;
            this.quantity = 1;
            this.shipDate = new DateTime();
            this.status = "placed";
            this.complete = true;
        }

        public int id { get; set; } 
        public int petId { get; set; } 
        public int quantity { get; set; } 
        public DateTime shipDate { get; set; } 
        public string status { get; set; } 
        public bool complete { get; set; } 
    }
}