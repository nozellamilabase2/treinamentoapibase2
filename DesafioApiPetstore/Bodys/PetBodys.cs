using System;
using System.Collections.Generic;
using System.Text;

namespace DesafioApiPetstore.Bodys
{
    public class Category    {
        public Category(int id, string name){
            this.id = id;
            this.name = name;
        }
        public int id { get; set; } 
        public string name { get; set; } 
    }

    public class Tag    {
        public Tag(int id, string name){
            this.id = id;
            this.name = name;
        }
        public int id { get; set; } 
        public string name { get; set; } 
    }

    public class PetsBodys    {

        public PetsBodys(){}
        public PetsBodys(int id, string name, string status){
            this.id = id;
            this.category = new Category(0, "medium"); //Hard code due to not be used in test assertions
            this.name = name;
            this.photoUrls = new List<string>(){"http://placeimg.com/640/480"}; //Hard code due to not be used in test assertions
            this.tags = new List<Tag>(){new Tag(0, "petLover")}; //Hard code due to not be used in test assertions
            this.status = status;
        }

        public int id { get; set; } 
        public Category category { get; set; } 
        public string name { get; set; } 
        public List<string> photoUrls { get; set; } 
        public List<Tag> tags { get; set; } 
        public string status { get; set; }

    }
}